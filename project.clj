(defproject sli/twc "0.1.0-SNAPSHOT"
  :description "A Tailwind CSS component library."
  :url "http://gitlab.com/docmenthol/twc"
  :license {:name "MIT"
            :url "https://mit-license.org/"}

  :min-lein-version "2.9.1"

  :dependencies [[org.clojure/clojure "1.10.0"]
                 [org.clojure/clojurescript "1.10.520"]
                 [org.clojure/core.async  "0.4.500"]
                 [reagent "0.9.0-rc2"]
                 [devcards "0.2.6"]
                 [sablono "0.8.6"]]

  :plugins [[lein-figwheel "0.5.19"]
            [lein-cljsbuild "1.1.7" :exclusions [[org.clojure/clojure]]]]

  :source-paths ["src"]

  :cljsbuild {:builds
              [{:id "dev"
                :source-paths ["src"]

                :figwheel {:on-jsload "twc.core/on-js-reload"
                           :open-urls ["http://localhost:4000/index.html"]}

                :compiler {:main twc.core
                           :asset-path "js/compiled/out"
                           :output-to  "resources/public/js/compiled/twc.js"
                           :output-dir "resources/public/js/compiled/out"
                           :source-map-timestamp true
                           :preloads [devtools.preload]}}
               {:id "min"
                :source-paths ["src"]
                :compiler {:output-to "resources/public/js/compiled/twc.js"
                           :main twc.core
                           :optimizations :advanced
                           :pretty-print false}}
               {:id "devcards"
                :source-paths ["src"]
                :figwheel {:devcards true}
                :compiler {:main twc.core
                           :asset-path "js/devcards/out"
                           :output-to  "resources/public/js/devcards/twc.js"
                           :output-dir "resources/public/js/devcards/out"
                           :source-map-timestamp true}}]}

  :figwheel {:server-port 4000
             :css-dirs ["resources/public/css"]
             :nrepl-port 8777}

  :profiles {:dev {:dependencies [[binaryage/devtools "0.9.10"]
                                  [figwheel-sidecar "0.5.19"]]
                   :source-paths ["src" "dev"]
                   :clean-targets ^{:protect false} ["resources/public/js/compiled"
                                                     :target-path]}})
