# twc

A Tailwind CSS component library for Reagent.

## Setup

Install Javascript dependencies:

```bash
$ yarn
```

Run devcards and CSS build:

```bash
$ yarn start
```

Open browser to [localhost:4000/devcards.html](http://localhost:4000/devcards.html).

## Theming

Theming is done with CSS. See [doc/theme.css](doc/theme.css) for the complete
theme currently used for devcard cards.

## License

Copyright © 2019 sli

Distributed under the MIT license.
