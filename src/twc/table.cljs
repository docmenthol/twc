(ns twc.table
  (:require [reagent.core :refer [as-element]]
            [clojure.string :refer [join]])
  (:require-macros [devcards.core :refer [defcard]]))

(defcard standard-table
  (let [columns [{:key :one   :display "One"}
                 {:key :two   :display "Two"}
                 {:key :three :display "Three"}
                 {:key :four  :display "Four"}
                 {:key :five  :display "Five"}]
        data (vec
              (for [i (range 1 6)]
                (zipmap
                  [:one :two :three :four :five]
                  (for [j (range 1 6)] (* i j)))))]
    (as-element [table {:columns columns :data data :striped true}])))

(defcard basic-table
  (let [columns [{:key :one   :display "One"}
                 {:key :two   :display "Two"}
                 {:key :three :display "Three"}
                 {:key :four  :display "Four"}
                 {:key :five  :display "Five"}]
        data (vec
              (for [i (range 1 6)]
                (zipmap
                  [:one :two :three :four :five]
                  (for [j (range 1 6)] (* i j)))))]
    (as-element [table {:columns columns :data data :basic true}])))

(defcard very-basic-table
  (let [columns [{:key :one   :display "One"}
                 {:key :two   :display "Two"}
                 {:key :three :display "Three"}
                 {:key :four  :display "Four"}
                 {:key :five  :display "Five"}]
        data (vec
              (for [i (range 1 6)]
                (zipmap
                  [:one :two :three :four :five]
                  (for [j (range 1 6)] (* i j)))))]
    (as-element [table {:columns columns :data data :basic "very"}])))

(defn table
  [{:keys [columns data table-class header-class row-class cell-class striped basic]}]
  [:div.twc-table
   {:class
    (concat [(when-not (= basic "very") "border") (when basic "basic")]
            table-class)}
   [:div.table-row {:class row-class}
    (for [[c-index c] (map-indexed vector columns)] ^{:key (join "-" ["header" (name (:key c))])}
      [:div.table-cell
        {:class (concat ["font-bold"
                         "border-b"
                         (when-not (or basic (= c-index (dec (count columns)))) "border-r")]
                        cell-class
                        header-class)}
        (:display c)])]
   (for [[r-index d] (map-indexed vector data)] ^{:key (join "-" ["row" r-index])}
     [:div.table-row {:class (concat row-class [(when (and striped (odd? r-index)) "twc-table-stripe")])}
      (for [c-index (range (count columns))] ^{:key (join "-" ["cell" r-index c-index])}
        (let [k (:key (nth columns c-index))]
          [:div.table-cell
            {:class
             (concat
               ["table-cell"
                (when-not (= r-index (dec (count columns))) "border-b")
                (when-not (or basic (= c-index (dec (count columns)))) "border-r")]
               cell-class)}
            (get d k)]))])])
