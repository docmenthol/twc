(ns twc.util
  (:require [clojure.string :refer [join]]))

(defn convert-classes
  [props]
  (let [classes (:class props)]
    (cond
      (vector? classes) (join " " classes)
      (string? classes) classes
      :else "")))
