(ns twc.button
  (:require [reagent.core :refer [as-element]]
            [twc.util :refer [convert-classes]])
  (:require-macros [devcards.core :refer [defcard]]))

(defcard basic-button
  (as-element
    [button
      {:on-click #(println "Clicked basic-button")}
      "Basic Button"]))

(defcard flat-button
  (as-element
    [flat-button
     {:on-click #(println "Clicked flat-button")}
     "Flat Button"]))

(defn button
  [buttondef & maybe-child]
  (let [[props child] (if (string? buttondef)
                          [{} buttondef]
                          [buttondef maybe-child])]
    [:button.twc-button.rounded.px-4.py-2
      (merge
        props
        {:class
         (concat
           (convert-classes (:class props))
           [(when (:disabled props) "opacity-50 cursor-not-allowed")])})
      child]))

(defn flat-button
  [buttondef & maybe-child]
  (let [[props child] (if (string? buttondef)
                          [{} buttondef]
                          [buttondef maybe-child])]
    [:button.twc-button.flat.rounded.px-4.py-2
      (merge
        props
        {:class
         (concat
           (convert-classes (:class props))
           ["bg-transparent border hover:border-transparent"
            (when (:disabled props) "opacity-50 cursor-not-allowed")])})
      child]))
