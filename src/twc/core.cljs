(ns twc.core
  (:require [twc.button]
            [twc.table]))

(enable-console-print!)

(defn on-js-reload
  []
  (println "JS reloaded."))
